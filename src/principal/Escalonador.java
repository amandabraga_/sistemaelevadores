package principal;

import java.util.ArrayList;

/**Classe que serve como monitor. Guarda os andares gerencia o acesso dos elevadores a estes
 * 
 * @author Amanda e Yves
 *
 */
public class Escalonador {
	
	private Andar andares[];
	
	private ArrayList<Integer> idAndaresOcupados;

	/** 
	* Construtor da classe
	* 
	* @param qtdeAndares	número inteiro contendo a quantidade andares do edifício
	*/
    public Escalonador(int qtdeAndares){
	
		this.andares = new Andar[qtdeAndares];
		this.idAndaresOcupados = new ArrayList<Integer>();
    }

    /** Adiciona um andar à lista de andares
     * 
     * @param numeroAndar	número do andar a ser posto na lista de andares
     * @param andar			instancia do Andar a ser posto na lista de andares
     */
	public void criaAndar(int numeroAndar, Andar andar){
		
		this.andares[numeroAndar] = andar;

	}
	
	/** Retorna a lista de andares
	 * 
	 * @return Retorna um vetor contendo os andares do edifício
	 */
	public Andar[] getAndares(){
		
		return andares;
	}
	
	/** Método para verificar se ainda há requisições pendentes em algum andar
	 * 
	 * @return	Retorna verdadeiro caso haja requisições pendentes e falso caso contrário
	 */
	public synchronized boolean haRequisicoesNosAndares(){
		
		for (Andar a : andares){
			
			if (a.quantidadeDeRequisicoesAndar() > 0)
				return true;
		}
		
		return false;
		
	}
	
	/**Calcula o andar ideal dado o andar atual do elevador
	 * 
	 * @param andarAtual	Andar atual do elevador
	 * @return	Retorna o andar escolhido para o elevador ir
	 */
	public synchronized int andarIdeal(int andarAtual){
		
		ArrayList<Integer> andaresMaisProximos = andaresMaisProximos(andarAtual);
		
		if (andaresMaisProximos.size() == 0)
			return -1;
		
		int andarEscolhido = andaresMaisProximos.get(0); 
		
		if (andaresMaisProximos.size() == 1){
			
			this.idAndaresOcupados.add(andarEscolhido);
			return andarEscolhido;
		}
		
		int maiorNumeroReqs = andares[andarEscolhido].quantidadeDeRequisicoesAndar();
		
		for (Integer i: andaresMaisProximos){
			
			if (i != andarEscolhido && andares[i].quantidadeDeRequisicoesAndar() > maiorNumeroReqs){
				
					andarEscolhido = i;
			}
		
		}
		
		this.idAndaresOcupados.add(andarEscolhido);
	
		return andarEscolhido;
		
	}
	
	/** Calcula o(s) andar(es) mais próximos do elevador que contém requisições pendentes
	 * 
	 * @param andarAtual	andar no qual o elevador se encontra
	 * @return	identificador do(s) andar(es) mais próximos do elevador que contém requisições pendentes
	 */
	private synchronized ArrayList<Integer> andaresMaisProximos(int andarAtual){
		
		int distancia = andares.length + 1;
		ArrayList<Integer> andaresIdeais = new ArrayList<Integer>();
		
		for (Andar a : andares){
			
			int distanciaLocal = Math.abs(a.getId() - andarAtual);
			
			if (a.quantidadeDeRequisicoesAndar() > 0 && distanciaLocal <= distancia && !this.idAndaresOcupados.contains(a.getId())){
	
				if (distanciaLocal < distancia){
					distancia = Math.abs(a.getId() - andarAtual);
					andaresIdeais.clear();
				}
				
				andaresIdeais.add(a.getId());

			}
				
		}
		
		return andaresIdeais;
		
	}
	
	/** Libera andar para que outros elevadores possam buscar suas requisições nele
	 * 
	 * @param andar	andar a ser liberado
	 */
	public synchronized void liberaAndarIdeal(int andar){
		
		if (this.idAndaresOcupados.contains(andar))
			this.idAndaresOcupados.remove((Integer) andar);
			
	}

}
