package principal;

import java.util.ArrayList;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

/**Lê e armazena as informações básicas do programa: elevadores, andares e requisições  
 * 
 * @author Amanda e Yves
 *
 */
public class Edificio {

	private int qtdeAndares, qtdeElevadores;

	Elevador elevadores[];
	
	Escalonador escalonador;
	

	public static void main(String[] args) {

		Edificio edificio = new Edificio();
		
		edificio.leArquivo("entrada.txt");
		
		/** Inicia threads **/
		for (int e = 0; e < edificio.qtdeElevadores; e++){
			
			edificio.elevadores[e].start();
		}
		
		/** Fluxo principal espera o término da execução das threads **/
		for (int e = 0; e < edificio.qtdeElevadores; e++){
			
			try {
				
				edificio.elevadores[e].join();
				
			} catch (InterruptedException exception) {
				
				exception.printStackTrace();
			}
		}
		
		System.out.println("Fim da execução");
		
	}
	
	/** Abre arquivo e inicializa variáveis de Edificio 
	 * 
	 * @param nomeArquivo	nome do arquivo que contém as requisições 
	 */
	
	private void leArquivo(String nomeArquivo){
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(nomeArquivo));
			
			String linhaSplit[] = br.readLine().split(" "); 
				
			this.qtdeAndares = stringToInteger(linhaSplit[0]);
			this.qtdeElevadores = stringToInteger(linhaSplit[1]);
			
			int capacidade = stringToInteger(linhaSplit[2]);
			
			linhaSplit = br.readLine().split(" ");
			
			criaAndares(br);
			
			criaElevadores(capacidade, linhaSplit);
	
			br.close();
		
		}catch(FileNotFoundException e){
			
			System.err.println("Arquivo nao encontrado"); 
			
		}catch(IOException e){
			
			System.err.println("Erro na leitura do arquivo"); 
		}
	
	}
	
	/** Instancia elevadores a partir dos dados do arquivo
	 * 
	 * @param capacidade 		número inteiro que contém a capacidade de usuários do elevador
	 * @param andaresIniciais	vetor contendo os andares iniciais dos elevadores como Strings
	 */
	private void criaElevadores(int capacidade, String andaresIniciais[]){
		
		this.elevadores = new Elevador[this.qtdeElevadores];
		
		try{
		
			for (int e = 0; e < this.qtdeElevadores; e++){
		
				this.elevadores[e] = new Elevador(e, stringToInteger(andaresIniciais[e]), capacidade, this.escalonador);
			}
			
		}catch(ArrayIndexOutOfBoundsException e){
			
			System.err.println("Escreva no arquivo o andar inicial de todos os elevadores");
			System.exit(0);
			
		}
			
	}
	
	/** Instancia andares a partir de dados do arquivo
	 * 
	 * @param br	buffer de leitura do arquivo
	 */
	private void criaAndares(BufferedReader br){
		
		this.escalonador = new Escalonador(this.qtdeAndares);
		
		String linhaSplit[];
		
		int numeroAndar = 0;
		
		try{
		
			while(br.ready()){
				
				linhaSplit = br.readLine().split(" ");				
					
				if (linhaSplit.length < 1 || linhaSplit[0].equals(""))
					break;			
				
				int numeroDeRequisicoes =  stringToInteger(linhaSplit[0]);
				
				Andar andar = new Andar(numeroAndar, numeroDeRequisicoes, arrayDeRequisicoes(numeroDeRequisicoes, linhaSplit));
				
				this.escalonador.criaAndar(numeroAndar++, andar);	
			
			}
			
			if (numeroAndar != this.qtdeAndares){
				
				System.err.println("Escreva no arquivo o numero de requisicoes de todos os andares");
				System.exit(1);
			}
			
		}catch(IOException e){
			
			System.err.println("Erro na leitura do arquivo"); 
		}
		
	}
	
	/**cria ArrayList de requisicoes dada uma linha do arquivo
	 * 
	 * @param linhaSplit	vetor contendo cada elemento de uma linha lida do arquivo 
	 * @return	ArrayList das requisicoes de um determinado andar
	 */
	
	private ArrayList<Integer> arrayDeRequisicoes(int numeroDeRequisicoes, String[] linhaSplit){

		ArrayList<Integer> requisicoes = new ArrayList<Integer>();
		
		for (int s = 1; s <= numeroDeRequisicoes; s++){
			
			int requisicao = stringToInteger(linhaSplit[s]);
			
			if (requisicao > this.qtdeAndares-1) requisicao = this.qtdeAndares-1;
			
			requisicoes.add(requisicao);			
		}	
		
		return requisicoes;
		
	}
	
	/** Método auxiliar para transformar um valor do tipo String para o tipo int
	 * 
	 * @param str String a ser passada para int
	 * @return	um número inteiro caso a conversão seja possível, e fim do programa caso contrário
	 */
	private int stringToInteger(String str){
		
		try{
			int retorno = Integer.parseInt(str);
			return retorno;
			
		}catch(NumberFormatException e){
			
			System.err.println("Todos os caracteres do arquivo precisam ser numeros inteiros");
			System.exit(2);
		}
		return 0;
	}

}