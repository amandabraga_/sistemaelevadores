package principal;

import java.util.ArrayList;

/**Possui os dados e métodos que um elevador executa. É a classe que estende Thread. 
 * 
 * @author Amanda e Yves
 *
 */
public class Elevador extends Thread{
	
	private int id;
	private int andarAtual;
	private static int capacidade;
	
	private Escalonador escalonador;
	
	private ArrayList<Integer> destinos = new ArrayList<Integer>();
	
	/** 
	* Construtor da classe.
	* 
	* @param id			identificador do elevador
	* @param andarAtual	andar inicial do elevador
	* @param capacidade	capacidade máxima de usuários do elevador		
	* @param escalonador	instância do escalonador(monitor)
	*/
    public Elevador(int id, int andarAtual, int capacidade, Escalonador escalonador){
    	
		this.id = id;
		this.andarAtual = andarAtual;
		Elevador.capacidade = capacidade;
		this.escalonador = escalonador;
    }
    
    /**
     * Método executado pelas threads
     */
    public void run(){
	
		System.out.println("  § Elevador " + id + " começou no andar " + andarAtual);
		
		int andarIdeal;
		
		while (this.escalonador.haRequisicoesNosAndares()){
			
				andarIdeal = this.escalonador.andarIdeal(this.andarAtual);
				
				if (andarIdeal < 0) return;
				
				this.destinos = this.escalonador.getAndares()[andarIdeal].pegaRequisicoes(capacidade);
				
				if (this.destinos == null) return;
				
				System.out.println(" >  Elevador " + id + " está no andar "+ this.andarAtual +
						" e vai atender "+ this.destinos.size() + " requisições do andar " + andarIdeal);
				
				this.escalonador.liberaAndarIdeal(andarIdeal);

				atendeRequisicoes(andarIdeal);
				
				System.out.println(" >  Elevador " + id + " serviu requisições vindas do andar "+ andarIdeal + 
						" e está no andar "+ this.andarAtual);
			
		}
		
		return;
    }
    
    /**	Chama os métodos que simulam o início e fim da viagem do elevador para cada requisição
     * 
     * @param andarIdeal	andar do qual o elevador recuperou as requisições que serão atendidas
     */
    private void atendeRequisicoes(int andarIdeal) {
		
    	for (Integer destino : this.destinos){
			
			andaElevador(andarIdeal, destino);
			
		}
		
		while (this.destinos.size() != 0){
								
			paraElevador(this.destinos.get(0));
			
		}
		
	}
    
    /** Verifica o andar no qual o elevador está 
     *
     * @return Retorna o andar atual do elevador
     */
    public int getAndarAtual(){
    	
    	return andarAtual;
    }
    
    /** Método que imprime log para simular um usuário entrando e apertando um botão para fazer uma viagem no elevador
     * 
     * @param andarEntrada	andar em que o elevador pegou o usuário
     * @param andarFinal	andar destino do usuário
     */
    public void andaElevador(int andarEntrada, int andarFinal){

		System.out.println("+ Elevador "+ this.id +" vai levar usuário de: "+ andarEntrada +" até "+ andarFinal);
		
	}
    
    /**	Método que completa uma requisição do elevador e imprime log de execução
     * 
     * @param andar	andar destino do usuário
     */
    public void paraElevador(int andar){	
		
		int qtdePessoas = 0;
		
		this.andarAtual = andar;

		while (this.destinos.contains(andar)){
			
			qtdePessoas++;			
			this.destinos.remove((Integer) andar);
		}

		System.out.println("- Elevador "+ this.id +" deixou "+ qtdePessoas +" pessoa(s) no andar "+ andar);

	}

}