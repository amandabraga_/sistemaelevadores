package principal;

import java.util.ArrayList;

/**Classe guarda informações e métodos referentes a um andar do edifício
 * 
 * @author Amanda e Yves
 *
 */
public class Andar {
	
	private int qtdeRequisicoes;
	
	private int id;
	
	private ArrayList<Integer> requisicoes;
	
	/** 
	* Construtor da classe
	* 
	* @param id 				identificador do andar
	* @param qtdeRequisicoes	quantidade de requisições do andar
	* @param requisicoes		lista de requisições do andar
	*/
	public Andar (int id, int qtdeRequisicoes, ArrayList<Integer> requisicoes){
		
		this.id = id;
		this.requisicoes = requisicoes;
		this.qtdeRequisicoes = qtdeRequisicoes;
		
	}
	
	/** Método para recuperar requisições do andar
	 * 			
	 * @return retorna a lista requisições do andar
	 */
	public ArrayList<Integer> requisicoesDoAndar(){

		return this.requisicoes;
	
	}
	
	/** Método para que um elevador pegue requisições do andar
	 * 
	 * @param capacidade	capacidade total do elevador
	 * @return	Retorna a lista das requisições do elevador
	 */
	public ArrayList<Integer> pegaRequisicoes(int capacidade){
						
			ArrayList<Integer> retorno = new ArrayList<Integer>();
	
			while (this.qtdeRequisicoes > 0 && retorno.size() < capacidade){
				
				retorno.add(this.requisicoes.get(0));
				this.requisicoes.remove(0);
				this.qtdeRequisicoes--;
			}
			
			return retorno;
	}
	
	/** Recupera a quantidade de requisições do andar
	 * @return Retorna a quantidade de requisições do andar
	 */
	public int quantidadeDeRequisicoesAndar(){
		
		return this.qtdeRequisicoes;
	}
	
	/**Retorna o identificador do andar
	 * @return	Retorna o identificador do andar
	 */
	public int getId(){
		
		return this.id;
	}

}