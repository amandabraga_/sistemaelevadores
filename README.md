# README #

### Descrição ###

Projeto desenvolvido em Java para simular o gerenciamento de requisições para elevadores.

### Como compilar ###

* No terminal/prompt de comando

1. Entre na pasta do projeto
2. Digite:
	
>>javac src/principal/*.java	
>>java -cp ./src/  principal.Edificio

* Para compilar e executar no Eclipse é necessário configurar o projeto caso a versão do Java na máquina não seja 1.8
